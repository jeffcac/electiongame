package com.omniture 
{
    import flash.display.*;
    import flash.events.*;
    import flash.external.*;
    import flash.geom.*;
    
    public dynamic class ActionSource_Module_ClickMap extends Object
    {
        
        {
            isExternalSet = false;
        }

        public function getPageName():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.pageName;
        }

        internal function getGeom(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            loc1 = this;
            loc2 = new Object();
            loc5 = loc1.parentGetBounds(arg1);
            loc3 = loc1.nodePos(arg1);
            loc4 = loc1.nodePos(arg1);
            loc1.nodeShift(arg1, loc5.xMin, loc5.yMin);
            loc6 = loc1.parentGetBounds(arg1);
            loc1.nodeShift(arg1, loc5.xMax, loc5.yMax);
            loc7 = loc1.parentGetBounds(arg1);
            loc1.nodeShift(arg1, loc3.x, loc3.y);
            loc3.x = loc3.x + (loc6.xMin - loc5.xMin);
            loc3.y = loc3.y + (loc6.yMin - loc5.yMin);
            loc4.x = loc4.x + (loc7.xMax - loc5.xMax);
            loc4.y = loc4.y + (loc7.yMax - loc5.yMax);
            loc1.parentLocalToGlobal(arg1, loc3);
            loc1.parentLocalToGlobal(arg1, loc4);
            loc2.x = Math.round(loc3.x);
            loc2.y = Math.round(loc3.y);
            loc2.w = Math.ceil(loc4.x - loc3.x);
            loc2.h = Math.ceil(loc4.y - loc3.y);
            return loc2;
        }

        public function getAccount():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.account;
        }

        public function getCharSet():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.charSet;
        }

        internal function nodePos(arg1:flash.display.DisplayObject):*
        {
            var loc1:*=undefined;
            loc1 = new Object();
            loc1.x = arg1.x;
            loc1.y = arg1.y;
            return loc1;
        }

        internal function indexChildren(arg1:flash.display.DisplayObjectContainer):String
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=NaN;
            var loc4:*=null;
            loc1 = this;
            loc2 = new String();
            loc3 = 0;
            while (loc3 < arg1.numChildren) 
            {
                loc4 = arg1.getChildAt(loc3);
                if (loc1.s.isSet(loc4)) 
                {
                    loc2 = loc2 + ("|" + loc1.getDOMID(loc4));
                    if (loc4 is flash.display.DisplayObjectContainer) 
                    {
                        loc2 = loc2 + loc1.indexChildren(flash.display.DisplayObjectContainer(loc4));
                    }
                }
                ++loc3;
            }
            return loc2;
        }

        public function getTrackClickMap():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.trackClickMap.toString();
        }

        public function getDOMIndex():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.getIndex();
        }

        public function getPageURL():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.pageURL;
        }

        internal function onMouseClick(arg1:flash.events.MouseEvent):void
        {
            var event:flash.events.MouseEvent;
            var m:Object;
            var e:Object;

            var loc1:*;
            m = null;
            e = null;
            event = arg1;
            m = this;
            try 
            {
                m.sendClickMapEvent(flash.display.InteractiveObject(event.target));
            }
            catch (e:*)
            {
            };
            return;
        }

        internal function getDOMID(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            loc1 = this;
            if (loc1.s.isSet(arg1)) 
            {
                loc2 = loc1.getGeom(arg1);
                return loc1.getFullPath(arg1) + "," + loc2.x + "," + loc2.y + "," + loc2.w + "," + loc2.h;
            }
            return "";
        }

        internal function onAddedToStage(arg1:flash.events.Event):void
        {
            var loc1:*=null;
            loc1 = this;
            loc1.s.root.addEventListener(flash.events.MouseEvent.CLICK, loc1.onMouseClick, true, 0, true);
            return;
        }

        internal function parentGetBounds(arg1:flash.display.DisplayObject):*
        {
            var loc1:*=undefined;
            var loc2:*=undefined;
            loc1 = arg1.parent.getBounds(arg1.parent);
            loc2 = new Object();
            loc2.xMin = loc1.x;
            loc2.yMin = loc1.y;
            loc2.xMax = loc1.x + loc1.width;
            loc2.yMax = loc1.y + loc1.height;
            return loc2;
        }

        internal function nodeShift(arg1:flash.display.DisplayObject, arg2:Number, arg3:Number):*
        {
            arg1.x = arg2;
            arg1.y = arg3;
            return;
        }

        internal function sendClickMapEvent(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=NaN;
            loc1 = this;
            loc2 = loc1.s.getMovieURL();
            loc3 = loc1.getMovieID();
            if (loc1.s.isSet(loc1.s.trackClickMap)) 
            {
                loc1.s.objectID = loc1.getObjectID(arg1);
            }
            if (loc1.s.autoTrack) 
            {
                if ((loc5 = (loc4 = loc2).indexOf("?")) >= 0) 
                {
                    loc4 = loc4.substr(0, loc5);
                }
                if (loc4.length > 100 - 23) 
                {
                    loc4 = loc4.substr(-(100 - 23));
                }
                loc1.s.trackLink(loc2, "o", "ActionSource.AutoTrack:" + loc4);
            }
            return;
        }

        public function getIndex():*
        {
            var loc1:*=null;
            var loc2:*=null;
            loc1 = this;
            if (loc1.s.isSet(loc1.s.movie)) 
            {
                loc2 = loc1.s.movie.stage.stageWidth + "," + loc1.s.movie.stage.stageHeight + loc1.indexChildren(loc1.s.movie);
                return loc2;
            }
            return "";
        }

        public function getMovieID():*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=NaN;
            loc1 = this;
            loc2 = loc1.getSWFURL();
            loc3 = s.movieID;
            if (!loc1.s.isSet(loc3) && loc1.s.isSet(loc2)) 
            {
                loc4 = loc2.lastIndexOf("/");
                loc5 = loc2.lastIndexOf(".");
                if (loc4 >= 0) 
                {
                    ++loc4;
                }
                else 
                {
                    loc4 = 0;
                }
                if (loc5 >= 0) 
                {
                    loc5 = loc5 - loc4;
                }
                else 
                {
                    loc5 = loc2.length;
                }
                loc3 = loc2.substr(loc4, loc5);
            }
            if (!loc1.s.isSet(loc3)) 
            {
                loc3 = "movieID undefined";
            }
            return loc3;
        }

        public function getObjectID(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            loc1 = this;
            loc2 = loc1.getMovieID();
            loc3 = "";
            loc3 = loc1.getFullPath(arg1);
            if (loc1.s.isSet(loc3)) 
            {
                loc3 = (loc1.s.isSet(loc2) ? loc2 : "") + ":" + loc3;
            }
            return loc3;
        }

        internal function getFullPath(arg1:flash.display.DisplayObject):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            loc3 = new Array();
            do 
            {
                loc3.splice(0, 0, arg1.name);
                arg1 = arg1.parent;
            }
            while (arg1.parent != null);
            loc1 = loc3.join(".");
            loc2 = loc1.substr(loc1.length - 4, 4);
            if (loc2 == ".frs" || loc2 == ".fds") 
            {
                loc1 = loc1.substr(0, loc1.length - 4);
            }
            return loc1;
        }

        internal function parentLocalToGlobal(arg1:flash.display.DisplayObject, arg2:Object):*
        {
            var loc1:*=undefined;
            loc1 = new flash.geom.Point(arg2.x, arg2.y);
            arg1.parent.localToGlobal(loc1);
            arg2.x = loc1.x;
            arg2.y = loc1.y;
            return;
        }

        public function getVersion():*
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.version;
        }

        public function getSWFURL():*
        {
            var loc1:*=null;
            loc1 = this;
            if (loc1.s.isSet(s.movie)) 
            {
                if (loc1.s.isSet(loc1.s.movie.loaderInfo) && loc1.s.isSet(loc1.s.movie.loaderInfo.loaderURL)) 
                {
                    return loc1.s.movie.loaderInfo.loaderURL;
                }
                if (loc1.s.isSet(loc1.s.movie._url)) 
                {
                    return loc1.s.movie._url;
                }
            }
            return "";
        }

        public function ActionSource_Module_ClickMap(arg1:Object)
        {
            var s:Object;
            var e:Object;
            var m:Object;

            var loc1:*;
            m = null;
            e = null;
            s = arg1;
            super();
            m = this;
            m.s = s;
            m.s.addEventListener(flash.events.Event.ADDED_TO_STAGE, m.onAddedToStage, false, 0, true);
            if (flash.external.ExternalInterface.available && !m.isExternalSet) 
            {
                m.isExternalSet = true;
                try 
                {
                    flash.external.ExternalInterface.addCallback("s_getDOMIndex", m.getDOMIndex);
                    flash.external.ExternalInterface.addCallback("s_getTrackClickMap", m.getTrackClickMap);
                    flash.external.ExternalInterface.addCallback("s_getAccount", m.getAccount);
                    flash.external.ExternalInterface.addCallback("s_getPageName", m.getPageName);
                    flash.external.ExternalInterface.addCallback("s_getPageURL", m.getPageURL);
                    flash.external.ExternalInterface.addCallback("s_getMovieID", m.getMovieID);
                    flash.external.ExternalInterface.addCallback("s_getVersion", m.getVersion);
                    flash.external.ExternalInterface.addCallback("s_getCharSet", m.getCharSet);
                    flash.external.ExternalInterface.addCallback("s_getSWFURL", m.getSWFURL);
                }
                catch (e:*)
                {
                };
            }
            return;
        }

        internal var s:Object;

        internal static var isExternalSet:Boolean=false;
    }
}

package com.omniture 
{
    import flash.display.*;
    import flash.events.*;
    import flash.external.*;
    import flash.net.*;
    import flash.system.*;
    import flash.utils.*;
    
    [Embed(source="ActionSource.swf", symbol = "com.omniture.ActionSource")]
    public dynamic class ActionSource extends flash.display.Sprite
    {
        public function replace(arg1:String, arg2:String, arg3:String):String
        {
            if (this.isSet(arg1)) 
            {
                if (arg1.indexOf(arg2) >= 0) 
                {
                    return arg1.split(arg2).join(arg3);
                }
            }
            return arg1;
        }

        internal function queryStringClickMap():String
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=null;
            var loc6:*=NaN;
            var loc7:*=null;
            loc1 = this;
            loc2 = "";
            loc3 = loc1.pageName;
            loc4 = 1;
            loc5 = loc1.objectID;
            loc6 = 1;
            loc7 = "FLASH";
            if (!loc1.isSet(loc5) && loc1.isSet(loc1.linkObject) && (loc1.isSet(loc1.linkObject, "name") || loc1.isSet(loc1.linkObject, "_name"))) 
            {
                loc5 = loc1.ClickMap.getObjectID(loc1.linkObject);
            }
            if (!loc1.isSet(loc3)) 
            {
                loc3 = loc1.pageURL;
                loc4 = 0;
            }
            if (loc1.isSet(loc1.trackClickMap) && loc1.isSet(loc3) && loc1.isSet(loc5) && loc1.isSet(loc7)) 
            {
                loc2 = loc2 + ("&pid=" + escape(loc3));
                loc2 = loc2 + (loc1.isSet(loc4) ? "&pidt=" + escape("" + loc4) : "");
                loc2 = loc2 + ("&oid=" + escape(loc5.substr(0, 100)));
                loc2 = loc2 + (loc1.isSet(loc6) ? "&oidt=" + escape("" + loc6) : "");
                loc2 = loc2 + ("&ot=" + escape(loc7));
            }
            return loc2;
        }

        internal function queryStringTechnology():String
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            loc1 = this;
            loc2 = "";
            loc3 = flash.system.Capabilities;
            if (loc1.isSet(loc3) && loc1.isSet(loc3.screenResolutionX) && loc1.isSet(loc3.screenResolutionY)) 
            {
                loc2 = loc2 + ("&s=" + loc3.screenResolutionX + "x" + loc3.screenResolutionY);
            }
            return loc2;
        }

        public function isSet(arg1:*, arg2:String=null):Boolean
        {
            var val:*;
            var mbr:String=null;
            var e:Object;

            var loc1:*;
            e = null;
            val = arg1;
            mbr = arg2;
            try 
            {
                if (mbr != null) 
                {
                    val = val[mbr];
                }
                return !(val == null) && !(val == undefined) && !("" + val == "NaN") && !(val == false) && !(val == "") && !(val == 0);
            }
            catch (e:*)
            {
            };
            return false;
        }

        public function flushBufferedRequests():*
        {
            var loc1:*=undefined;
            loc1 = this;
            if (loc1.isSet(loc1.account)) 
            {
                loc1._flushBufferedRequests(loc1.account);
            }
            return;
        }

        internal function startDelayTrackingInterval():*
        {
            var loc1:*=null;
            loc1 = this;
            loc1.delayTrackingInterval = flash.utils.setInterval(delayTrackingDone, loc1.delayTracking);
            return;
        }

        internal function doTrackOnLoad():*
        {
            var loc1:*=null;
            loc1 = this;
            if (!loc1.isSet(loc1.account) || !loc1.isSet(loc1.movie)) 
            {
                return;
            }
            flash.utils.clearInterval(loc1.trackOnLoadInterval);
            if (loc1._trackOnLoad && !loc1.onLoadTracked) 
            {
                loc1.onLoadTracked = true;
                loc1.track();
            }
            return;
        }

        internal function queryStringAccountVariables():String
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=NaN;
            var loc4:*=NaN;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=undefined;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=undefined;
            var loc11:*=undefined;
            var loc12:*=undefined;
            loc1 = this;
            loc2 = "";
            loc10 = "";
            loc11 = "";
            loc12 = "";
            if (loc1.isSet(loc1.linkType)) 
            {
                loc10 = loc1.linkTrackVars;
                loc11 = loc1.linkTrackEvents;
            }
            else if (loc1.isSet(loc1.pe)) 
            {
                loc12 = loc1.pe.substr(0, 1).toUpperCase() + loc1.pe.substr(1);
                if (loc1.isSet(loc1[loc12])) 
                {
                    loc10 = loc1[loc12].trackVars;
                    loc11 = loc1[loc12].trackEvents;
                }
            }
            if (loc1.isSet(loc10)) 
            {
                loc10 = "," + loc10 + "," + loc1.requiredVarList.join(",") + ",";
            }
            if (loc1.isSet(loc11)) 
            {
                loc11 = "," + loc11 + ",";
            }
            loc3 = 0;
            while (loc3 < loc1.accountVarList.length) 
            {
                loc5 = loc1.accountVarList[loc3];
                loc6 = loc1[loc5];
                loc8 = loc5.substr(0, 4);
                loc9 = loc5.substr(4);
                if (loc1.isSet(loc6) && (!loc1.isSet(loc10) || loc10.indexOf("," + loc5 + ",") >= 0)) 
                {
                    var loc13:*=loc5;
                    switch (loc13) 
                    {
                        case "dynamicVariablePrefix":
                        {
                            loc5 = "D";
                            break;
                        }
                        case "visitorID":
                        {
                            loc5 = "vid";
                            break;
                        }
                        case "pageURL":
                        {
                            loc5 = "g";
                            break;
                        }
                        case "referrer":
                        {
                            loc5 = "r";
                            break;
                        }
                        case "vmk":
                        {
                            loc5 = "vmt";
                            break;
                        }
                        case "charSet":
                        {
                            loc5 = "ce";
                            break;
                        }
                        case "visitorNamespace":
                        {
                            loc5 = "ns";
                            break;
                        }
                        case "cookieDomainPeriods":
                        {
                            loc5 = "cdp";
                            break;
                        }
                        case "cookieLifetime":
                        {
                            loc5 = "cl";
                            break;
                        }
                        case "currencyCode":
                        {
                            loc5 = "cc";
                            break;
                        }
                        case "channel":
                        {
                            loc5 = "ch";
                            break;
                        }
                        case "transactionID":
                        {
                            loc5 = "xact";
                            break;
                        }
                        case "campaign":
                        {
                            loc5 = "v0";
                            break;
                        }
                        case "events":
                        {
                            if (loc1.isSet(loc11)) 
                            {
                                loc7 = loc6.split(",");
                                loc6 = "";
                                loc4 = 0;
                                while (loc4 < loc7.length) 
                                {
                                    if (loc11.indexOf("," + loc7[loc4] + ",") >= 0) 
                                    {
                                        loc6 = loc6 + ((loc1.isSet(loc6) ? "," : "") + loc7[loc4]);
                                    }
                                    ++loc4;
                                }
                            }
                            break;
                        }
                        default:
                        {
                            if (loc1.isNumber(loc9)) 
                            {
                                if (loc8 != "prop") 
                                {
                                    if (loc8 != "eVar") 
                                    {
                                        if (loc8 == "hier") 
                                        {
                                            loc5 = "h" + loc9;
                                            loc6 = loc6.substr(0, 255);
                                        }
                                    }
                                    else 
                                    {
                                        loc5 = "v" + loc9;
                                    }
                                }
                                else 
                                {
                                    loc5 = "c" + loc9;
                                }
                            }
                            break;
                        }
                    }
                    if (loc1.isSet(loc6)) 
                    {
                        loc2 = loc2 + ("&" + escape(loc5) + "=" + (loc5.substr(0, 3) == "pev" ? loc6 : escape(loc6)));
                    }
                }
                ++loc3;
            }
            return loc2;
        }

        public function isNumber(arg1:*):Boolean
        {
            return !isNaN(parseInt(arg1));
        }

        public function flushBufferedRequest(arg1:String, arg2:String):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=null;
            loc1 = this;
            loc2 = loc1.getBufferedRequests();
            if (loc1.isSet(loc2)) 
            {
                loc4 = 0;
                while (loc4 < loc2.data.list.length) 
                {
                    if ((loc3 = loc2.data.list[loc4]).account == arg1 && loc3.id == arg2) 
                    {
                        loc5 = loc2.data.list[loc4].request;
                        loc2.data.list[loc4].account = "";
                        loc2.data.list[loc4].id = "";
                        loc2.data.list[loc4].request = "";
                        loc2.flush();
                        loc1.makeRequest("", "", loc5, "");
                    }
                    ++loc4;
                }
            }
            return;
        }

        public function set trackOnLoad(arg1:Boolean):*
        {
            this._trackOnLoad = arg1;
            if (this._trackOnLoad) 
            {
                this.setTrackOnLoadInterval();
            }
            return;
        }

        internal function variableOverridesApply(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=NaN;
            var loc3:*=null;
            loc1 = this;
            loc2 = 0;
            while (loc2 < loc1.accountVarList.length) 
            {
                loc3 = loc1.accountVarList[loc2];
                if (loc1.isSet(arg1[loc3]) || loc1.isSet(arg1["!" + loc3])) 
                {
                    loc1[loc3] = arg1[loc3];
                }
                ++loc2;
            }
            loc2 = 0;
            while (loc2 < loc1.accountConfigList.length) 
            {
                loc3 = loc1.accountConfigList[loc2];
                if (loc1.isSet(arg1[loc3]) || loc1.isSet(arg1["!" + loc3])) 
                {
                    loc1[loc3] = arg1[loc3];
                }
                ++loc2;
            }
            return;
        }

        internal function setVariableCallHandler():*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=NaN;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=NaN;
            loc1 = this;
            var loc10:*=0;
            var loc11:*=loc1;
            for (loc3 in loc11) 
            {
                if (loc3.substr(0, 5) != "_svc_") 
                {
                    continue;
                }
                loc5 = loc3.split("_");
                if (!(loc1.isSet(loc5) && loc5.length >= 4)) 
                {
                    continue;
                }
                if (loc5[3] == "dot" && loc5.length > 4) 
                {
                    loc5[2] = loc5[2] + ("_dot_" + loc5[4]);
                    loc6 = 5;
                    while (loc6 < loc5.length) 
                    {
                        loc5[loc6 - 2] = loc5[loc6];
                        ++loc6;
                    }
                }
                loc8 = null;
                if (loc1.isSet(loc7)) 
                {
                    loc9 = 0;
                    while (loc9 < loc7.length) 
                    {
                        if (loc7[loc9].methodName == loc5[2]) 
                        {
                            loc8 = loc7[loc9];
                        }
                        ++loc9;
                    }
                }
                if (!loc1.isSet(loc8)) 
                {
                    loc7 = new Array();
                    (loc8 = new Object()).methodName = loc5[2];
                    loc7[0] = loc8;
                }
                if (loc5[3] == "call") 
                {
                    if (loc1.isSet(loc1[loc3])) 
                    {
                        loc8.call = true;
                    }
                    loc1[loc3] = null;
                    continue;
                }
                if (!(loc5[3] == "param" && loc5.length > 4 && loc1.isSet(loc5[4]))) 
                {
                    continue;
                }
                if (!loc1.isSet(loc8.paramList)) 
                {
                    loc8.paramList = new Array();
                }
                loc8.paramList[loc5[4]] = loc1[loc3];
            }
            if (loc1.isSet(loc7) && loc7.length > 0) 
            {
                loc9 = 0;
                while (loc9 < loc7.length) 
                {
                    loc8 = loc7[loc9];
                    if (loc1.isSet(loc8.methodName) && loc1.isSet(loc8.call)) 
                    {
                        if ((loc5 = loc8.methodName.split("_dot_")).length > 1) 
                        {
                            loc2 = loc1[loc5[0]];
                            loc4 = loc5[1];
                        }
                        else 
                        {
                            loc2 = loc1;
                            loc4 = loc8.methodName;
                        }
                        if (loc1.isSet(loc2[loc4])) 
                        {
                            if (loc1.isSet(loc8.paramList) && loc8.paramList.length > 0) 
                            {
                                if (loc8.paramList.length != 1) 
                                {
                                    if (loc8.paramList.length != 2) 
                                    {
                                        if (loc8.paramList.length == 3) 
                                        {
                                            loc1["_svc_result_" + loc8.methodName] = (loc10 = loc2)[loc4](loc8.paramList[0], loc8.paramList[1], loc8.paramList[2]);
                                        }
                                    }
                                    else 
                                    {
                                        loc1["_svc_result_" + loc8.methodName] = (loc10 = loc2)[loc4](loc8.paramList[0], loc8.paramList[1]);
                                    }
                                }
                                else 
                                {
                                    loc1["_svc_result_" + loc8.methodName] = (loc10 = loc2)[loc4](loc8.paramList[0]);
                                }
                            }
                            else 
                            {
                                loc1["_svc_" + loc8.methodName + "_result"] = (loc10 = loc2)[loc4]();
                            }
                        }
                    }
                    ++loc9;
                }
            }
            return;
        }

        public function get movie():Object
        {
            return this._movie;
        }

        internal function getBufferedRequests():*
        {
            var bufferedRequests:Object;
            var s:Object;

            var loc1:*;
            s = null;
            bufferedRequests = null;
            s = this;
            if (!s.isSet(s.disableBufferedRequests)) 
            {
                bufferedRequests = flash.net.SharedObject.getLocal("s_br", "/");
            }
            if (!s.isSet(bufferedRequests)) 
            {
                bufferedRequests = s.bufferedRequests;
                if (!s.isSet(bufferedRequests)) 
                {
                    s.bufferedRequests = new Object();
                    s.bufferedRequests.flush = function ():*
                    {
                        return;
                    }
                    bufferedRequests = s.bufferedRequests;
                }
            }
            if (!s.isSet(bufferedRequests.data)) 
            {
                bufferedRequests.data = new Object();
            }
            if (!s.isSet(bufferedRequests.data.list)) 
            {
                bufferedRequests.data.list = new Array();
            }
            return bufferedRequests;
        }

        internal function modulesInit():*
        {
            var loc1:*=null;
            loc1 = this;
            loc1.ClickMap = new com.omniture.ActionSource_Module_ClickMap(loc1);
            loc1.Media = new com.omniture.ActionSource_Module_Media(loc1);
            loc1.modulesUpdate();
            return;
        }

        internal function modulesUpdate():*
        {
            var loc1:*=null;
            var loc2:*=null;
            loc1 = this;
            if (loc1.isSet(loc1.Media)) 
            {
                if (loc1.isSet(loc1._moduleMediaVariables)) 
                {
                    var loc3:*=0;
                    var loc4:*=loc1._moduleMediaVariables;
                    for (loc2 in loc4) 
                    {
                        if (!loc1.isSet(loc1._moduleMediaVariables[loc2])) 
                        {
                            continue;
                        }
                        if (loc2 == "autoTrack") 
                        {
                            if (("" + loc1._moduleMediaVariables[loc2]).toLowerCase() != "true") 
                            {
                                loc1._moduleMediaVariables[loc2] = false;
                            }
                            else 
                            {
                                loc1._moduleMediaVariables[loc2] = true;
                            }
                        }
                        loc1.Media[loc2] = loc1._moduleMediaVariables[loc2];
                    }
                }
                loc1.Media.autoTrack = loc1.Media.autoTrack;
            }
            return;
        }

        public function track(arg1:Object=null, arg2:String=""):*
        {
            this._track(arg1, arg2);
            return;
        }

        public function set moduleMediaVariables(arg1:Object):*
        {
            this._moduleMediaVariables = arg1;
            this.modulesUpdate();
            return;
        }

        internal function initPre():*
        {
            if (parent == null || !(flash.utils.getQualifiedClassName(parent) == "fl.livepreview::LivePreviewParent")) 
            {
                this.visible = false;
            }
            else 
            {
                this.flashLivePreview = true;
            }
            this.addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
            flash.utils.setInterval(this.setVariableCallHandler, 1000);
            return;
        }

        public function get moduleMediaVariables():Object
        {
            return this._moduleMediaVariables;
        }

        public function getMovieURL():String
        {
            var loc1:*=null;
            var loc2:*=null;
            loc1 = this;
            loc2 = loc1.callJavaScript("function s_ActionSource_wl(){return window.location.href;}");
            if (loc1.isSet(loc2)) 
            {
                return loc2;
            }
            if (loc1.isSet(loc1.movie)) 
            {
                if (loc1.flashASVersion > 2 && loc1.isSet(loc1.movie.loaderInfo) && loc1.isSet(loc1.movie.loaderInfo.loaderURL)) 
                {
                    return loc1.movie.loaderInfo.loaderURL;
                }
                if (loc1.isSet(loc1.movie._url)) 
                {
                    return loc1.movie._url;
                }
            }
            return "";
        }

        internal function onAddedToStage(arg1:flash.events.Event):*
        {
            this.movie = root;
            return;
        }

        internal function setTrackOnLoadInterval():void
        {
            this.trackOnLoadInterval = flash.utils.setInterval(this.doTrackOnLoad, 50);
            return;
        }

        internal function getMovieReferrer():String
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.callJavaScript("" + "function s_ActionSource_r(){" + "\tvar " + "\t\tr = \'\'," + "\t\tw = window," + "\t\te," + "\t\tp," + "\t\tl," + "\t\te;" + "\tif ((w) && (w.document)) {" + "\t\tr = w.document.referrer;" + "\t\ttry {" + "\t\t\tp = w.parent;" + "\t\t\tl = w.location;" + "\t\t\twhile ((p) && (p.location) && (p.location != l) && (p.location.host == l.host)) {" + "\t\t\t\tw = p;" + "\t\t\t\tp = w.parent;" + "\t\t\t}" + "\t\t} catch (e) {}" + "\t\tif ((w) && (w.document)) {" + "\t\t\tr = w.document.referrer;" + "\t\t}" + "\t}" + "\treturn r;" + "}");
        }

        internal function initPost():*
        {
            return;
        }

        public function trackLink(arg1:*, arg2:String, arg3:String, arg4:Object=null):*
        {
            this._trackLink(arg1, arg2, arg3, arg4);
            return;
        }

        internal function _flushBufferedRequests(arg1:String):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            loc1 = this;
            loc2 = loc1.getBufferedRequests();
            if (loc1.isSet(loc2)) 
            {
                loc4 = 0;
                while (loc4 < loc2.data.list.length) 
                {
                    if ((loc3 = loc2.data.list[loc4]).account == arg1) 
                    {
                        loc1.flushBufferedRequest(arg1, loc3.id);
                    }
                    ++loc4;
                }
            }
            return;
        }

        internal function delayTrackingDone():*
        {
            var loc1:*=null;
            loc1 = this;
            flash.utils.clearInterval(loc1.delayTrackingInterval);
            loc1.delayTrackingStage = 2;
            loc1.flushRequestList();
            return;
        }

        public function ActionSource()
        {
            var loc1:*=null;
            var loc2:*=NaN;
            var loc3:*=null;
            var loc4:*=null;
            flashASVersion = 3;
            flashLivePreview = false;
            trackLocal = true;
            debugTracking = false;
            _trackOnLoad = false;
            onLoadTracked = false;
            trackCalled = false;
            super();
            loc1 = this;
            loc1.initPre();
            loc1.version = "FAS-2.4";
            if (loc1.isSet(_root)) 
            {
                loc1.movie = _root;
            }
            else if (loc1.isSet(root)) 
            {
                loc1.movie = root;
            }
            loc3 = getVersion();
            loc4 = loc3.split(" ");
            loc1.flashVersion = parseInt(loc4[1].substr(0, 1));
            loc1.requestNum = 0;
            loc1.requestList = new Array();
            loc1.lastRequest = "";
            loc1.requiredVarList = ["dynamicVariablePrefix", "visitorID", "vmk", "charSet", "visitorNamespace", "cookieDomainPeriods", "cookieLifetime", "pageName", "pageURL", "referrer", "currencyCode"];
            loc1.accountVarList = ["purchaseID", "variableProvider", "channel", "server", "pageType", "transactionID", "campaign", "state", "zip", "events", "products"];
            loc2 = (loc1.requiredVarList.length - 1);
            while (loc2 >= 0) 
            {
                loc1.accountVarList.unshift(loc1.requiredVarList[loc2]);
                --loc2;
            }
            loc2 = 1;
            while (loc2 <= 50) 
            {
                loc1.accountVarList.push("prop" + loc2);
                loc1.accountVarList.push("eVar" + loc2);
                loc1.accountVarList.push("hier" + loc2);
                ++loc2;
            }
            loc1.accountVarList.push("pe");
            loc1.accountVarList.push("pev1");
            loc1.accountVarList.push("pev2");
            loc1.accountVarList.push("pev3");
            loc1.requiredVarList.push("pe");
            loc1.requiredVarList.push("pev1");
            loc1.requiredVarList.push("pev2");
            loc1.requiredVarList.push("pev3");
            loc1.accountConfigList = ["linkObject", "linkURL", "linkName", "linkType", "trackDownloadLinks", "trackExternalLinks", "trackClickMap", "linkLeaveQueryString", "linkTrackVars", "linkTrackEvents", "trackingServer", "trackingServerSecure", "dc", "movieID", "autoTrack", "delayTracking", "trackLocal", "debugTracking"];
            loc1.modulesInit();
            loc1.initPost();
            return;
        }

        public function clearVars():*
        {
            var loc1:*=null;
            var loc2:*=NaN;
            var loc3:*=null;
            loc1 = this;
            loc2 = 0;
            while (loc2 < accountVarList.length) 
            {
                loc3 = loc1.accountVarList[loc2];
                if (loc3.substr(0, 4) == "prop" || loc3.substr(0, 4) == "eVar" || loc3.substr(0, 4) == "hier" || loc3 == "channel" || loc3 == "events" || loc3 == "purchaseID" || loc3 == "transactionID" || loc3 == "products" || loc3 == "state" || loc3 == "zip" || loc3 == "campaign") 
                {
                    loc1[loc3] = undefined;
                }
                ++loc2;
            }
            return;
        }

        internal function variableOverridesBuild(arg1:Object):*
        {
            var loc1:*=null;
            var loc2:*=NaN;
            var loc3:*=null;
            loc1 = this;
            loc2 = 0;
            while (loc2 < loc1.accountVarList.length) 
            {
                loc3 = loc1.accountVarList[loc2];
                if (!loc1.isSet(arg1[loc3])) 
                {
                    arg1[loc3] = loc1[loc3];
                    if (!loc1.isSet(arg1[loc3])) 
                    {
                        arg1["!" + loc3] = 1;
                    }
                }
                ++loc2;
            }
            loc2 = 0;
            while (loc2 < loc1.accountConfigList.length) 
            {
                loc3 = loc1.accountConfigList[loc2];
                if (!loc1.isSet(arg1[loc3])) 
                {
                    arg1[loc3] = loc1[loc3];
                    if (!loc1.isSet(arg1[loc3])) 
                    {
                        arg1["!" + loc3] = 1;
                    }
                }
                ++loc2;
            }
            return;
        }

        internal function flushRequestList():*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            loc1 = this;
            while (loc1.requestNum < loc1.requestList.length) 
            {
                if (loc1.isSet(loc1.debugTracking)) 
                {
                    loc2 = "ActionSource Debug: " + loc1.requestList[loc1.requestNum];
                    loc3 = loc1.requestList[loc1.requestNum].split("&");
                    loc4 = 0;
                    while (loc4 < loc3.length) 
                    {
                        loc2 = loc2 + ("\n\t" + unescape(loc3[loc4]));
                        ++loc4;
                    }
                    // trace(loc2);
                }
                loc1.requestURL(loc1.requestList[loc1.requestNum]);
                loc1.lastRequest = loc1.requestList[loc1.requestNum];
                var loc5:*;
                var loc6:*=((loc5 = loc1).requestNum + 1);
                loc5.requestNum = loc6;
            }
            return;
        }

        internal function bufferRequest(arg1:String, arg2:String, arg3:String):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=NaN;
            loc2 = (loc1 = this).getBufferedRequests();
            if (loc1.isSet(loc2)) 
            {
                loc5 = -1;
                loc4 = 0;
                while (loc4 < loc2.data.list.length) 
                {
                    if (loc2.data.list[loc4].id != arg2) 
                    {
                        if (!loc1.isSet(loc2.data.list[loc4].id)) 
                        {
                            loc5 = loc4;
                        }
                    }
                    else 
                    {
                        loc2.data.list[loc4].request = arg3;
                        arg3 = "";
                    }
                    ++loc4;
                }
                if (loc1.isSet(arg3)) 
                {
                    (loc3 = new Object()).account = arg1;
                    loc3.id = arg2;
                    loc3.request = arg3;
                    if (loc5 >= 0) 
                    {
                        loc2.data.list[loc5] = loc3;
                    }
                    else 
                    {
                        loc2.data.list.push(loc3);
                    }
                }
                loc2.flush();
            }
            return;
        }

        internal function requestURL(arg1:*):*
        {
            var loc1:*=null;
            loc1 = new flash.net.URLRequest(arg1);
            flash.net.sendToURL(loc1);
            return;
        }

        internal function _trackLink(arg1:*, arg2:String, arg3:String, arg4:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            if ((loc1 = this).isSet(arg1) && typeof arg1 == "string") 
            {
                loc2 = arg1;
                arg1 = new Object();
                arg1.url = loc2;
            }
            loc1.linkObject = arg1;
            loc1.linkType = arg2;
            loc1.linkName = arg3;
            loc1.track(arg4);
            return;
        }

        public function set movie(arg1:Object):*
        {
            this._movie = arg1;
            if (this.isSet(this._movie)) 
            {
                if (!this.flashLivePreview) 
                {
                    if (this.flashASVersion < 3) 
                    {
                        this._movie.s_s = this;
                    }
                    this.modulesUpdate();
                }
            }
            return;
        }

        internal function queryStringLinkTracking():String
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=NaN;
            var loc6:*=NaN;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=NaN;
            loc1 = this;
            loc2 = loc1.linkType;
            loc3 = loc1.linkURL;
            loc4 = loc1.linkName;
            loc8 = "";
            if (!loc1.isSet(loc3) && loc1.isSet(loc1.linkObject)) 
            {
                if (loc1.isSet(loc1.linkObject, "url")) 
                {
                    loc3 = loc1.linkObject.url;
                }
                else if (loc1.isSet(loc1.linkObject, "URL")) 
                {
                    loc3 = loc1.linkObject.URL;
                }
                else if (loc1.isSet(loc1.linkObject, "href")) 
                {
                    loc3 = loc1.linkObject.href;
                }
                else if (loc1.isSet(loc1.linkObject, "HREF")) 
                {
                    loc3 = loc1.linkObject.HREF;
                }
                else if (loc1.isSet(loc1.linkObject, "htmlText")) 
                {
                    if ((loc5 = loc1.linkObject.htmlText.toLowerCase().indexOf("href=")) >= 0) 
                    {
                        loc5 = loc5 + 5;
                        if ((loc7 = loc1.linkObject.htmlText.substr(loc5, 1)) == "\"" || loc7 == "\'") 
                        {
                            ++loc5;
                            if ((loc6 = loc1.linkObject.htmlText.toLowerCase().indexOf(loc7, loc5)) >= 0) 
                            {
                                if (--loc6 > loc5) 
                                {
                                    loc3 = loc1.linkObject.htmlText.substr(loc5, loc6 - loc5 + 1);
                                }
                            }
                        }
                    }
                }
            }
            if (loc1.isSet(loc2) && (loc1.isSet(loc3) || loc1.isSet(loc4))) 
            {
                loc2 = loc2.toLowerCase();
                if (!(loc2 == "d") && !(loc2 == "e")) 
                {
                    loc2 = "o";
                }
                if (loc1.isSet(loc3) && !loc1.isSet(loc1.linkLeaveQueryString)) 
                {
                    if ((loc9 = loc3.indexOf("?")) >= 0) 
                    {
                        loc3 = loc3.substr(0, loc9);
                    }
                }
                loc8 = (loc8 = (loc8 = loc8 + ("&pe=lnk_" + escape(loc2))) + (loc1.isSet(loc3) ? "&pev1=" + escape(loc3) : "")) + (loc1.isSet(loc4) ? "&pev2=" + escape(loc4) : "");
            }
            return loc8;
        }

        internal function _track(arg1:Object, arg2:String):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=NaN;
            var loc9:*=null;
            loc1 = this;
            loc3 = new Date();
            loc4 = Math.floor(Math.random() * 1e+13);
            loc5 = "s" + Math.floor(loc3.getTime() / 10800000) % 10 + loc4;
            loc6 = "" + loc3.getDate() + "/" + loc3.getMonth() + "/" + loc3.getFullYear() + " " + loc3.getHours() + ":" + loc3.getMinutes() + ":" + loc3.getSeconds() + " " + loc3.getDay() + " " + loc3.getTimezoneOffset();
            loc7 = "t=" + escape(loc6);
            if (loc1.isSet(loc1.flashLivePreview)) 
            {
                return;
            }
            if (loc1.isSet(loc1.otherVariables)) 
            {
                loc8 = 0;
                while (loc8 < loc1.accountVarList.length) 
                {
                    loc9 = loc1.accountVarList[loc8];
                    if (loc1.isSet(loc1.otherVariables[loc9])) 
                    {
                        loc1[loc9] = loc1.otherVariables[loc9];
                    }
                    ++loc8;
                }
                loc8 = 0;
                while (loc8 < loc1.accountConfigList.length) 
                {
                    loc9 = loc1.accountConfigList[loc8];
                    if (loc1.isSet(loc1.otherVariables[loc9])) 
                    {
                        loc1[loc9] = loc1.otherVariables[loc9];
                    }
                    ++loc8;
                }
            }
            if (loc1.isSet(arg1)) 
            {
                loc2 = new Object();
                loc1.variableOverridesBuild(loc2);
                loc1.variableOverridesApply(arg1);
            }
            if (loc1.isSet(loc1.usePlugins) && loc1.isSet(loc1.doPlugins)) 
            {
                loc1.doPlugins(loc1);
            }
            if (loc1.isSet(loc1.account)) 
            {
                if (!loc1.isSet(loc1.pageURL)) 
                {
                    loc1.pageURL = loc1.getMovieURL();
                }
                if (!loc1.isSet(loc1.referrer) && !loc1.isSet(loc1._1_referrer)) 
                {
                    loc1.referrer = loc1.getMovieReferrer();
                    loc1._1_referrer = 1;
                }
                loc7 = (loc7 = (loc7 = (loc7 = loc7 + loc1.queryStringAccountVariables()) + loc1.queryStringLinkTracking()) + loc1.queryStringClickMap()) + loc1.queryStringTechnology();
                loc1.makeRequest(loc5, loc7, "", arg2);
            }
            if (loc1.isSet(arg1)) 
            {
                loc1.variableOverridesApply(loc2);
            }
            loc1.referrer = undefined;
            loc1.pe = undefined;
            loc1.pev1 = undefined;
            loc1.pev2 = undefined;
            loc1.pev3 = undefined;
            loc1.linkObject = undefined;
            loc1.linkURL = undefined;
            loc1.linkName = undefined;
            loc1.linkType = undefined;
            loc1.objectID = undefined;
            if (loc1.isSet(loc1.account)) 
            {
                if (!loc1.isSet(arg2) && !loc1.isSet(loc1.trackCalled)) 
                {
                    loc1.trackCalled = true;
                    loc1.flushBufferedRequests();
                }
            }
            return;
        }

        public function get trackOnLoad():Boolean
        {
            return this._trackOnLoad;
        }

        internal function callJavaScript(arg1:String):*
        {
            var s:Object;
            var script:String;
            var e:Object;

            var loc1:*;
            s = null;
            e = null;
            script = arg1;
            s = this;
            try 
            {
                if (s.isSet(flash.external.ExternalInterface) && s.isSet(flash.external.ExternalInterface.available) && s.isSet(flash.external.ExternalInterface.call)) 
                {
                    return flash.external.ExternalInterface.call(script);
                }
            }
            catch (e:*)
            {
            };
            return null;
        }

        internal function getVersion():String
        {
            return flash.system.Capabilities.version;
        }

        internal function makeRequest(arg1:String, arg2:String, arg3:String, arg4:String):*
        {
            var loc1:*=null;
            var loc2:*=undefined;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=NaN;
            loc2 = (loc1 = this).getMovieURL();
            if (!loc1.isSet(arg3)) 
            {
                if (loc1.isSet(loc1.trackingServer)) 
                {
                    loc3 = loc1.trackingServer;
                    if (loc1.isSet(loc1.trackingServerSecure) && loc2.toLowerCase().substr(0, 6) == "https:") 
                    {
                        loc3 = loc1.trackingServerSecure;
                    }
                }
                else 
                {
                    loc4 = loc1.visitorNamespace;
                    if (!loc1.isSet(loc4)) 
                    {
                        if ((loc5 = (loc4 = loc1.account).indexOf(",")) >= 0) 
                        {
                            loc4 = loc4.substr(0, loc5);
                        }
                        loc4 = loc4.split("_").join("-");
                    }
                    loc3 = loc4 + "." + (loc1.isSet(loc1.dc) ? loc1.dc : 112) + ".2o7.net";
                }
                if (loc2.toLowerCase().substr(0, 6) != "https:") 
                {
                    arg3 = "http://";
                }
                else 
                {
                    arg3 = "https://";
                }
                arg3 = arg3 + (loc3 + "/b/ss/" + loc1.account + "/" + (loc1.mobile ? "5.0" : "0") + "/" + loc1.version + "-AS" + loc1.flashASVersion + "/" + arg1 + "?AQB=1&ndh=1&" + arg2 + "&AQE=1");
                if (loc1.isSet(arg4)) 
                {
                    loc1.bufferRequest(loc1.account, arg4, arg3);
                    return;
                }
            }
            if (loc2.toLowerCase().substr(0, 6) == "https:" && arg3.toLowerCase().substr(0, 5) == "http:") 
            {
                arg3 = "https:" + arg3.substr(5);
            }
            if (loc1.isSet(loc1.trackLocal) || loc1.flashVersion < 8 || !loc1.isSet(loc2) || loc2.toLowerCase().substr(0, 4) == "http") 
            {
                loc1.requestList.push(arg3);
                if (!loc1.isSet(loc1.delayTracking) || loc1.isSet(loc1.delayTrackingStage) && loc1.delayTrackingStage == 2) 
                {
                    loc1.flushRequestList();
                }
                else if (loc1.isSet(loc1.delayTracking) && !loc1.isSet(loc1.delayTrackingStage)) 
                {
                    loc1.delayTrackingStage = 1;
                    loc1.startDelayTrackingInterval();
                }
            }
            return;
        }

        public var debugTracking:Boolean=false;

        public var movieID:String;

        internal var trackOnLoadInterval:Number;

        internal var accountVarList:Array;

        internal var delayTrackingInterval:Number;

        public var ClickMap:com.omniture.ActionSource_Module_ClickMap;

        public var _movie:Object;

        public var account:String;

        internal var _trackOnLoad:Boolean=false;

        public var pageName:String;

        internal var _root:Object;

        internal var requiredVarList:Array;

        public var mobile:Boolean;

        internal var delayTrackingStage:Number;

        public var requestList:Array;

        public var Media:com.omniture.ActionSource_Module_Media;

        public var flashVersion:Number;

        internal var onLoadTracked:Boolean=false;

        public var trackingServerSecure:String;

        internal var flashRoot:Object;

        public var visitorNamespace:String;

        internal var trackCalled:Boolean=false;

        internal var accountConfigList:Array;

        public var dc:Number;

        internal var requestNum:Number;

        public var lastRequest:String;

        public var pageURL:String;

        public var trackClickMap:Boolean;

        public var trackLocal:Boolean=true;

        internal var flashLivePreview:Boolean=false;

        public var otherVariables:Object;

        public var charSet:String;

        public var autoTrack:Boolean;

        public var trackingServer:String;

        internal var _moduleMediaVariables:Object;

        public var version:String;

        public var flashASVersion:Number=3;

        public var delayTracking:Number;
    }
}

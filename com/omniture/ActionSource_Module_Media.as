package com.omniture 
{
    import flash.utils.*;
    
    public dynamic class ActionSource_Module_Media extends Object
    {
        public function close(arg1:String):*
        {
            this.event(arg1, 0, -1);
            return;
        }

        public function play(arg1:String, arg2:Number):*
        {
            var monitor:Object;
            var offset:Number;
            var media:Object;
            var name:String;
            var m:Object;

            var loc1:*;
            m = null;
            media = null;
            monitor = null;
            name = arg1;
            offset = arg2;
            m = this;
            m.event(name, 1, offset);
            monitor = new Object();
            monitor.m = m;
            monitor.node = m.cleanName(name);
            monitor.watch = function ():*
            {
                var loc1:*=null;
                var loc2:*=null;
                var loc3:*=null;
                loc1 = this.m;
                loc2 = this.node;
                loc3 = loc1.s.isSet(loc2) && loc1.s.isSet(loc1.list) && loc1.s.isSet(loc1.list[loc2]) ? loc1.list[loc2] : null;
                if (loc1.s.isSet(loc3)) 
                {
                    if (loc3.lastEventType == 1) 
                    {
                        loc1.event(loc3.name, 3, -1);
                    }
                }
                else 
                {
                    this.node = null;
                }
                return;
            }
            m.startMonitor(monitor);
            return;
        }

        public function listenerFLVPlayback_stateChange(arg1:*):*
        {
            this.listenerFLVPlayback.stateChange(arg1);
            return;
        }

        public function set autoTrack(arg1:Boolean):*
        {
            this._autoTrack = arg1;
            if (this._autoTrack && !this.autoTrackDone && this.s.isSet(this.s.movie)) 
            {
                this.autoTrackDone = true;
                this.attach(this.s.movie);
            }
            return;
        }

        internal function cleanName(arg1:String):String
        {
            var loc1:*=null;
            loc1 = this;
            return loc1.s.replace(loc1.s.replace(loc1.s.replace(arg1, "\n", ""), "\r", ""), "--**--", "");
        }

        internal function autoEvent(arg1:String, arg2:Number, arg3:String, arg4:Number, arg5:Number, arg6:Object):*
        {
            var loc1:*=null;
            arg1 = (loc1 = this).cleanName(arg1);
            if (loc1.s.isSet(arg1) && loc1.s.isSet(arg2) && loc1.s.isSet(arg3)) 
            {
                if (!loc1.s.isSet(loc1.list) || !loc1.s.isSet(loc1.list[arg1])) 
                {
                    loc1.open(arg1, arg2, arg3, arg6);
                }
                loc1.event(arg1, arg4, arg5);
            }
            return;
        }

        internal function _open(arg1:String, arg2:Number, arg3:String, arg4:Object):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            loc1 = this;
            loc2 = new Object();
            loc3 = new Date();
            loc4 = "";
            arg1 = loc1.cleanName(arg1);
            arg2 = Math.floor(arg2);
            if (!loc1.s.isSet(arg2)) 
            {
                arg2 = 1;
            }
            if (loc1.s.isSet(arg1) && loc1.s.isSet(arg2) && loc1.s.isSet(arg3)) 
            {
                if (!loc1.s.isSet(loc1.list)) 
                {
                    loc1.list = new Object();
                }
                if (loc1.s.isSet(loc1.list[arg1])) 
                {
                    loc1.close(arg1);
                }
                if (loc1.s.isSet(arg4)) 
                {
                    loc4 = "" + arg4;
                }
                var loc6:*=0;
                var loc7:*=loc1.list;
                for (loc5 in loc7) 
                {
                    if (!(loc1.s.isSet(loc1.list[loc5]) && loc1.list[loc5].playerID == loc4)) 
                    {
                        continue;
                    }
                    loc1.close(loc1.list[loc5].name);
                }
                loc2.name = arg1;
                loc2.length = arg2;
                loc2.playerName = loc1.cleanName(loc1.s.isSet(loc1.playerName) ? loc1.playerName : arg3);
                loc2.playerID = loc4;
                loc2.timePlayed = 0;
                loc2.timestamp = Math.floor(loc3.getTime() / 1000);
                loc2.lastEventType = 0;
                loc2.lastEventTimestamp = loc2.timestamp;
                loc2.lastEventOffset = 0;
                loc2.session = "";
                loc2.eventList = new Object();
                loc1.list[arg1] = loc2;
            }
            return;
        }

        internal function startMonitor(arg1:Object):*
        {
            var monitor:Object;
            var monitorNum:Number;
            var m:Object;
            var nextMonitorNum:Number;

            var loc1:*;
            m = null;
            monitorNum = NaN;
            nextMonitorNum = NaN;
            monitor = arg1;
            m = this;
            nextMonitorNum = 0;
            if (m.s.isSet(m.monitorList)) 
            {
                nextMonitorNum = -1;
                monitorNum = 0;
                while (monitorNum < m.monitorList.length) 
                {
                    if (m.s.isSet(m.monitorList[monitorNum])) 
                    {
                        if (m.s.isSet(m.monitorList[monitorNum].node) && m.s.isSet(monitor) && m.s.isSet(monitor.node) && m.monitorList[monitorNum].node == monitor.node) 
                        {
                            return;
                        }
                    }
                    else if (nextMonitorNum < 0) 
                    {
                        nextMonitorNum = monitorNum;
                    }
                    ++monitorNum;
                }
                if (nextMonitorNum < 0) 
                {
                    nextMonitorNum = m.monitorList.length;
                }
            }
            else 
            {
                m.monitorList = new Array();
            }
            monitor.update = function (arg1:*):*
            {
                if (arg1.m == null || arg1.m == undefined || arg1.m.s == null || arg1.m.s == undefined || arg1.node == null || arg1.node == undefined) 
                {
                    flash.utils.clearInterval(arg1.interval);
                    arg1.m.monitorList[arg1.num] = null;
                }
                else 
                {
                    arg1.watch();
                }
                return;
            }
            monitor.interval = flash.utils.setInterval(monitor.update, 5000, monitor);
            monitor.num = nextMonitorNum;
            m.monitorList[monitor.num] = monitor;
            return;
        }

        public function listenerFLVPlayback_complete(arg1:*):*
        {
            this.listenerFLVPlayback.complete(arg1);
            return;
        }

        public function listenerMedia_complete(arg1:*):*
        {
            this.listenerMedia.complete(arg1);
            return;
        }

        public function get autoTrack():Boolean
        {
            return this._autoTrack;
        }

        internal function event(arg1:String, arg2:Number, arg3:Number):*
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=null;
            loc1 = this;
            loc3 = new Date();
            loc4 = Math.floor(loc3.getTime() / 1000);
            loc5 = "--**--";
            arg1 = loc1.cleanName(arg1);
            loc2 = loc1.s.isSet(arg1) && loc1.s.isSet(loc1.list) && loc1.s.isSet(loc1.list[arg1]) ? loc1.list[arg1] : null;
            if (loc1.s.isSet(loc2)) 
            {
                if (arg2 == 3 || !(arg2 == loc2.lastEventType) && (!(arg2 == 2) || loc2.lastEventType == 1)) 
                {
                    if (loc1.s.isSet(arg2)) 
                    {
                        if (arg3 < 0 && loc2.lastEventTimestamp > 0) 
                        {
                            arg3 = loc4 - loc2.lastEventTimestamp + loc2.lastEventOffset;
                            arg3 = arg3 < loc2.length ? arg3 : (loc2.length - 1);
                        }
                        arg3 = Math.floor(arg3);
                        if ((arg2 == 2 || arg2 == 3) && loc2.lastEventOffset < arg3) 
                        {
                            loc2.timePlayed = loc2.timePlayed + (arg3 - loc2.lastEventOffset);
                        }
                        if (arg2 == 3) 
                        {
                            if (loc2.lastEventType != 1) 
                            {
                                loc1.event(arg1, 1, arg3);
                            }
                        }
                        else 
                        {
                            loc2.session = loc2.session + ((arg2 != 1 ? "E" : "S") + arg3);
                            loc2.lastEventType = arg2;
                        }
                        loc2.lastEventTimestamp = loc4;
                        loc2.lastEventOffset = arg3;
                        loc1.s.pe = "media";
                        loc1.s.pev3 = "" + escape(loc2.name) + loc5 + loc2.length + loc5 + escape(loc2.playerName) + loc5 + loc2.timePlayed + loc5 + loc2.timestamp + loc5 + loc2.session + (arg2 != 3 ? "" : "E" + arg3);
                        loc1.s.track(null, "Media." + arg1);
                    }
                    else 
                    {
                        loc1.event(arg1, 2, -1);
                        loc1.list[arg1] = 0;
                        loc1.s.flushBufferedRequest(loc1.s.account, "Media." + arg1);
                    }
                }
            }
            return;
        }

        public function listenerMedia_click(arg1:*):*
        {
            this.listenerMedia.click(arg1);
            return;
        }

        public function open(arg1:String, arg2:Number, arg3:String, arg4:Object=null):*
        {
            this._open(arg1, arg2, arg3, arg4);
            return;
        }

        public function stop(arg1:String, arg2:Number):*
        {
            this.event(arg1, 2, arg2);
            return;
        }

        public function listenerMedia_change(arg1:*):*
        {
            this.listenerMedia.change(arg1);
            return;
        }

        internal function attach(arg1:Object):*
        {
            var member:String;
            var node:Object;
            var childNum:Number;
            var monitor:Object;
            var m:Object;

            var loc1:*;
            m = null;
            member = null;
            childNum = NaN;
            monitor = null;
            node = arg1;
            m = this;
            if (m.s.isSet(node)) 
            {
                if (m.s.isSet(node, "addEventListener") && m.s.isSet(node, "isFLVCuePointEnabled")) 
                {
                    if (!m.s.isSet(m.listenerFLVPlayback)) 
                    {
                        m.listenerFLVPlayback = new Object();
                        m.listenerFLVPlayback.m = m;
                        m.listenerFLVPlayback.playerName = "Flash FLVPlayback";
                        m.listenerFLVPlayback.handleEvent = function (arg1:Object, arg2:Number):*
                        {
                            var loc1:*=null;
                            var loc2:*=null;
                            var loc3:*=NaN;
                            var loc4:*=NaN;
                            loc1 = this.m;
                            if (loc1.s.isSet(loc1.autoTrack) && loc1.s.isSet(arg1)) 
                            {
                                if (loc1.s.flashASVersion > 2) 
                                {
                                    loc2 = arg1.source;
                                }
                                else 
                                {
                                    loc2 = arg1.contentPath;
                                }
                                loc3 = arg1.totalTime;
                                loc4 = arg1.playheadTime;
                                loc1.autoEvent(loc2, loc3, this.playerName, arg2, loc4, arg1);
                            }
                            return;
                        }
                        m.listenerFLVPlayback.stateChange = function (arg1:*):*
                        {
                            var loc1:*=null;
                            var loc2:*=NaN;
                            var loc3:*=null;
                            loc1 = this.m;
                            loc2 = -1;
                            if (loc1.s.isSet(arg1) && loc1.s.isSet(arg1.target)) 
                            {
                                loc3 = arg1.target;
                                if (loc1.s.isSet(loc3, "state")) 
                                {
                                    if (loc3.state != "playing") 
                                    {
                                        if (loc3.state == "stopped" || loc3.state == "paused" || loc3.state == "buffering" || loc3.state == "rewinding" || loc3.state == "seeking") 
                                        {
                                            loc2 = 2;
                                        }
                                    }
                                    else 
                                    {
                                        loc2 = 1;
                                    }
                                    if (loc2 >= 0) 
                                    {
                                        this.handleEvent(arg1.target, loc2);
                                    }
                                }
                            }
                            return;
                        }
                        m.listenerFLVPlayback.complete = function (arg1:*):*
                        {
                            if (this.m.s.isSet(arg1)) 
                            {
                                this.handleEvent(arg1.target, 0);
                            }
                            return;
                        }
                    }
                    if (m.s.flashASVersion > 2) 
                    {
                        node.addEventListener("complete", m.listenerFLVPlayback_complete);
                        node.addEventListener("stateChange", m.listenerFLVPlayback_stateChange);
                    }
                    else 
                    {
                        node.addEventListener("complete", m.listenerFLVPlayback);
                        node.addEventListener("stateChange", m.listenerFLVPlayback);
                    }
                    monitor = new Object();
                    monitor.m = m;
                    monitor.node = node;
                    monitor.watch = function ():*
                    {
                        var loc1:*=null;
                        var loc2:*=null;
                        loc1 = this.m;
                        loc2 = this.node;
                        if (loc1.s.isSet(loc2.state) && loc2.state == "playing") 
                        {
                            this.m.listenerFLVPlayback.handleEvent(loc2, 3);
                        }
                        return;
                    }
                    m.startMonitor(monitor);
                }
                else if (m.s.isSet(node, "addEventListener") && m.s.isSet(node, "addCuePoint")) 
                {
                    if (!m.s.isSet(m.listenerMedia)) 
                    {
                        m.listenerMedia = new Object();
                        m.listenerMedia.m = m;
                        m.listenerMedia.playerName = "Flash Media";
                        m.listenerMedia.handleEvent = function (arg1:Object, arg2:Number):*
                        {
                            var loc1:*=null;
                            var loc2:*=null;
                            var loc3:*=NaN;
                            var loc4:*=NaN;
                            loc1 = this.m;
                            if (loc1.s.isSet(loc1.autoTrack) && loc1.s.isSet(arg1)) 
                            {
                                loc2 = arg1.contentPath;
                                loc3 = arg1.totalTime;
                                loc4 = arg1.playheadTime;
                                loc1.autoEvent(loc2, loc3, this.playerName, arg2, loc4, arg1);
                            }
                            return;
                        }
                        m.listenerMedia.complete = function (arg1:*):*
                        {
                            if (this.m.s.isSet(arg1)) 
                            {
                                this.handleEvent(arg1.target, 0);
                            }
                            return;
                        }
                        m.listenerMedia.click = function (arg1:*):*
                        {
                            if (this.m.s.isSet(arg1) && this.m.s.isSet(arg1.target)) 
                            {
                                this.handleEvent(arg1.target, this.m.s.isSet(arg1.target.playing) ? 1 : 2);
                            }
                            return;
                        }
                        m.listenerMedia.change = function (arg1:*):*
                        {
                            if (this.m.s.isSet(arg1) && this.m.s.isSet(arg1.target)) 
                            {
                                this.handleEvent(arg1.target, this.m.s.isSet(arg1.target.playing) ? 1 : 2);
                            }
                            return;
                        }
                        m.listenerMedia.scrubbing = function (arg1:*):*
                        {
                            if (this.m.s.isSet(arg1)) 
                            {
                                this.handleEvent(arg1.target, 2);
                            }
                            return;
                        }
                    }
                    if (m.s.flashASVersion > 2) 
                    {
                        node.addEventListener("complete", m.listenerMedia_complete);
                        node.addEventListener("click", m.listenerMedia_click);
                        node.addEventListener("change", m.listenerMedia_change);
                        node.addEventListener("scrubbing", m.listenerMedia_scrubbing);
                    }
                    else 
                    {
                        node.addEventListener("complete", m.listenerMedia);
                        node.addEventListener("click", m.listenerMedia);
                        node.addEventListener("change", m.listenerMedia);
                        node.addEventListener("scrubbing", m.listenerMedia);
                    }
                    monitor = new Object();
                    monitor.m = m;
                    monitor.node = node;
                    monitor.watch = function ():*
                    {
                        var loc1:*=null;
                        var loc2:*=null;
                        loc1 = this.m;
                        loc2 = this.node;
                        if (loc1.s.isSet(loc2.playing)) 
                        {
                            this.m.listenerMedia.handleEvent(loc2, 3);
                        }
                        return;
                    }
                    m.startMonitor(monitor);
                }
                else if (m.s.flashASVersion > 2) 
                {
                    if (m.s.isSet(node, "numChildren") && m.s.isSet(node, "getChildAt")) 
                    {
                        childNum = 0;
                        while (childNum < node.numChildren) 
                        {
                            m.attach(node.getChildAt(childNum));
                            ++childNum;
                        }
                    }
                }
                else 
                {
                    var loc2:*=0;
                    var loc3:*=node;
                    for (member in loc3) 
                    {
                        if (!(m.s.isSet(node[member]) && m.s.isSet(node[member]._name) && node[member]._name == member && "" + node + "." + member == "" + node[member])) 
                        {
                            continue;
                        }
                        m.attach(node[member]);
                    }
                }
            }
            return;
        }

        public function listenerMedia_scrubbing(arg1:*):*
        {
            this.listenerMedia.scrubbing(arg1);
            return;
        }

        public function ActionSource_Module_Media(arg1:Object)
        {
            var loc1:*=null;
            super();
            loc1 = this;
            loc1.s = arg1;
            loc1.monitorList = null;
            return;
        }

        internal var monitorList:Array;

        internal var _autoTrack:Boolean;

        internal var list:Object;

        public var trackVars:String;

        public var playerName:String;

        public var trackEvents:String;

        internal var s:Object;

        internal var autoTrackDone:Boolean;
    }
}
